CREATE TABLE "info_customizacoes" (
  "id_info_customizacoes" bigint,
  "ref" varchar(8000),
  "value" varchar(8000),
  "id_itens" bigint,
  PRIMARY KEY ("id_info_customizacoes")
);

CREATE TABLE "pagamentos" (
  "id_pagamentos" bigint,
  "tipo" varchar(255),
  "valor" numeric(10,2),
  "nsu" varchar(255),
  "metodo" varchar(255),
  "merchant_key" bigint,
  "id_pedidos" bigint,
  PRIMARY KEY ("id_pagamentos")
 );

CREATE TABLE "pedidos" (
  "id_pedidos" bigint,
  "data_criacao" timestamp,
  "data_ultima_modificacao" timestamp,
  "data_submissao" timestamp,
  "status" varchar(400),
  "pedido_criado_linx" timestamp,
  "data_criacao_linx" timestamp,
  "id_clientes" bigint,
  "id_origens" bigint,
  PRIMARY KEY ("id_pedidos")
);



CREATE TABLE "clientes" (
  "id_clientes" bigint,
  "nome" varchar(100),
  "sobrenome" varchar(800),
  "sexo" varchar(1),
  "aniversario" timestamp,
  "cpf" bigint,
  "cnpj" bigint,
  "email" varchar(200),
  "tel_fixo" bigint,
  "celular" bigint,
  "apelido" varchar(200),
  "data_registro" timestamp,
  "rg" bigint,
  "razao_social" varchar(500),
  "nome_fantasma" varchar(300),
  "inscricao_estadual" varchar(300),
  PRIMARY KEY ("id_clientes")
);


CREATE TABLE "enderecos" (
  "id_enderecos" bigint,
  "logradouro" varchar(255),
  "numero" int,
  "complemento" varchar(8000),
  "bairro" varchar(255),
  "cidade" varchar(255),
  "estado" varchar(255),
  "pais" varchar(255),
  "cep" int,
  "id_clientes" bigint,
  PRIMARY KEY ("id_enderecos")
);



CREATE TABLE "descontos" (
  "id_descontos" bigint,
  "tipo" varchar(255),
  "cupom" varchar(255),
  "valor" numeric(10,2),
  "id_promocao" bigint,
  "id_pedidos" bigint,
  PRIMARY KEY ("id_descontos")
);


CREATE TABLE "endereco_pedidos" (
  "id_pedidos" bigint,
  "id_enderecos" bigint,
  "tipo_endereco" varchar(100),
  "metodo_entrega" varchar(100),
  "valor_frete" int,
  "nome_pessoa_entrega" varchar(300),
  "celular" bigint,
  "tel_fixo" bigint,
  PRIMARY KEY ("id_pedidos", "id_enderecos")
);


CREATE TABLE "origens" (
  "id_origens" bigint,
  "local" varchar(300),
  PRIMARY KEY ("id_origens")
);

CREATE TABLE "composicoes" (
  "id_composicoes" bigint,
  "material" varchar(20),
  "porcentagem" smallint,
  PRIMARY KEY ("id_composicoes")
);

CREATE TABLE "descricoes" (
  "id_descricoes" bigint,
  "tem_logo" varchar(1),
  "nome" varchar(200),
  "estilo" varchar(100),
  "cor" varchar(100),
  "tamanho" varchar(20),
  "id_composicoes" bigint,
  PRIMARY KEY ("id_descricoes")
);


CREATE TABLE "itens" (
  "id_itens" bigint,	
  "id_produtos" bigint,
  "id_pedidos" bigint,
  "id_skus" bigint,
  "valor_pago" numeric(10,2),
  "valor_original" numeric(10,2),
  "valor_oferta" numeric(10,2),
  "quantidade" int,
  "descontado" boolean,
  "pra_presente" boolean,
  "data_entrega" timestamp,
  PRIMARY KEY ("id_itens")
);



CREATE TABLE "skus" (
  "id_skus" bigint,
  "nome" varchar(8000),
  "descricao" varchar(8000),
  "id_produtos" bigint,
  PRIMARY KEY ("id_skus")
);



CREATE TABLE "produtos" (
  "id_produtos" bigint,
  "tipo" varchar(100),
  "id_descricoes" bigint,
  PRIMARY KEY ("id_produtos")
);



ALTER TABLE "info_customizacoes"
	ADD CONSTRAINT "Info_Customizacoes_FK_id_itens" FOREIGN KEY ("id_itens") REFERENCES "itens" ("id_itens");


ALTER TABLE "pagamentos"
	ADD CONSTRAINT "pagamentos_FK_id_pedidos" FOREIGN KEY ("id_pedidos") REFERENCES "pedidos" ("id_pedidos");


ALTER TABLE "pedidos"
	ADD CONSTRAINT "pedidos_FK_id_clientes" FOREIGN KEY ("id_clientes") REFERENCES "clientes" ("id_clientes"),
	ADD	CONSTRAINT "pedidos_FK_id_origens" FOREIGN KEY ("id_origens") REFERENCES "origens" ("id_origens");


ALTER TABLE "enderecos"
	ADD CONSTRAINT "enderecos_FK_id_clientes" FOREIGN KEY ("id_clientes") REFERENCES "clientes" ("id_clientes");
	

ALTER TABLE "descontos"
	ADD CONSTRAINT "descontos_FK_id_pedidos" FOREIGN KEY ("id_pedidos") REFERENCES "pedidos" ("id_pedidos");
	

ALTER TABLE "endereco_pedidos"
	ADD CONSTRAINT "endereco_pedidos_FK_id_pedidos" FOREIGN KEY ("id_pedidos") REFERENCES "pedidos" ("id_pedidos"),
	ADD	CONSTRAINT "endereco_pedidos_FK_id_enderecos" FOREIGN KEY ("id_enderecos") REFERENCES "enderecos" ("id_enderecos");	

ALTER TABLE "descricoes"
	ADD CONSTRAINT "descricoes_FK_id_composicoes" FOREIGN KEY ("id_composicoes") REFERENCES "composicoes" ("id_composicoes");

ALTER TABLE "itens"
	ADD CONSTRAINT "itens_FK_id_pedidos" FOREIGN KEY ("id_pedidos") REFERENCES "pedidos" ("id_pedidos"),
	ADD	CONSTRAINT "itens_FK_id_produtos" FOREIGN KEY ("id_produtos") REFERENCES "produtos" ("id_produtos"),
	ADD	CONSTRAINT "itens_FK_id_skus" FOREIGN KEY ("id_skus") REFERENCES "skus" ("id_skus");

ALTER TABLE "skus"
	ADD CONSTRAINT "skus_FK_id_produtos" FOREIGN KEY ("id_produtos") REFERENCES "produtos" ("id_produtos");

ALTER TABLE "produtos"
	ADD CONSTRAINT "produtos_FK_id_descricoes" FOREIGN KEY ("id_descricoes") REFERENCES "descricoes" ("id_descricoes");

